import React, {Component} from 'react';
import styles from './Style';
import {
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Image,
  ImageBackground,
  Platform,
  TextInput,
} from 'react-native';
import {Block, theme} from 'galio-framework';
import {Images, argonTheme} from '../../constants';
import {HeaderHeight} from '../../constants/utils';
import {
  Container,
  Header,
  Content,
  Footer,
  FooterTab,
  Button,
  Badge,
  Icon,
  Text,
} from 'native-base';
import {color} from 'react-native-reanimated';

const {width, height} = Dimensions.get('screen');
const thumbMeasure = (width - 48 - 32) / 3;

export default class Home extends Component {
  render() {
    var menuESS = [];

    var itemESS = [
      {
        id: 1,
        icon: Images.IconMenu.Personal,
        text: 'Information',
      },
      {
        id: 2,
        icon: Images.IconMenu.Medical,
        text: 'Medical Claim',
      },
      {
        id: 3,
        icon: Images.IconMenu.Leave,
        text: 'Leave',
      },
      {
        id: 4,
        icon: Images.IconMenu.Travel,
        text: 'Travel',
      },
    ];

    for (let i = 0; i < itemESS.length; i++) {
      var item = itemESS[i];
      menuESS.push(
        <Block flex style={{alignItems: 'center', padding: 5, marginRight: 5}}>
          <Image
            source={item.icon}
            style={{
              width: 64,
              height: 64,
              margin: 0,
              alignItems: 'center',
              marginBottom: 10
            }}
          />
          <Text style={{color: item.color, fontSize: 10, textAlign: 'center'}}>{item.text}</Text>
        </Block>,
      );
    }

    return (
      <Container style={{backgroundColor: '#ffffff'}}>
        <Block flex>
          <Block row style={styles.headerBox}>
            <ImageBackground
              source={Images.HomeHeader1}
              style={styles.headerBackground1Container}
              imageStyle={styles.headerBackground1}>
              <Block style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                <Image
                  source={Images.HomeHeader2}
                  style={styles.headerBackground2Container}
                />
              </Block>
              <Block row middle>
                <Image source={Images.Logo} style={styles.logo} />
              </Block>
            </ImageBackground>
          </Block>

          <ScrollView
            showsVerticalScrollIndicator={false}
            style={{width, marginTop: '-30%'}}>
            <Block style={styles.profileCard}>
              <Block row>
                <Block flex1 style={{marginRight: 40}}>
                  <Image source={Images.ProfileNuril} style={styles.avatar} />
                </Block>

                <Block flex2 style={{paddingTop: 10}}>
                  <Text style={styles.profileName}>MOHAMMAD NURIL UMAM</Text>
                  <Text style={styles.profileTitle}>.NET Programmer, IT</Text>
                </Block>
              </Block>

              <Block row style={{marginTop: 15}}>
                <Block middle style={styles.divider} />
              </Block>

              <Block row>
                <Block flex1 style={{paddingTop: 10, marginRight: 10}}>
                  <Text style={styles.profileBio}>SITE LOCATION</Text>
                  <Text style={styles.profileBio}>NIK SITE</Text>
                </Block>
                <Block flex3 style={{paddingTop: 10}}>
                  <Text style={styles.profileBioVal}>
                    : PT J RESOURCES NUSANTARA
                  </Text>
                  <Text style={styles.profileBioVal}>: JRN0291</Text>
                </Block>
              </Block>
            </Block>

            <Block flex style={{marginTop: 5, padding: 15}}>
              <Block row>
                <Text style={styles.titleMenu}>EMPLOYEE SELF SERVICE</Text>
              </Block>

              <Block row style={{paddingTop: 10}}>
                {menuESS}

                {/* <Block flex>
                  <Button active vertical style={{backgroundColor: '#fff'}}>
                    <Icon active name="apps" style={{color: '#434e52'}} />
                    <Text style={{color: '#434e52'}}>Home</Text>
                  </Button>
                </Block>

                <Block flex>
                  <Button active vertical style={{backgroundColor: '#fff'}}>
                    <Icon active name="apps" style={{color: '#434e52'}} />
                    <Text style={{color: '#434e52'}}>Home</Text>
                  </Button>
                </Block>

                <Block flex>
                  <Button active vertical style={{backgroundColor: '#fff'}}>
                    <Icon active name="apps" style={{color: '#434e52'}} />
                    <Text style={{color: '#434e52'}}>Home</Text>
                  </Button>
                </Block>

                <Block flex>
                  <Button active vertical style={{backgroundColor: '#fff'}}>
                    <Icon active name="apps" style={{color: '#434e52'}} />
                    <Text style={{color: '#434e52'}}>Home</Text>
                  </Button>
                </Block> */}
              </Block>
            </Block>

            <Block
              row
              style={{
                paddingVertical: 14,
                alignItems: 'baseline',
              }}></Block>
          </ScrollView>
        </Block>

        {/* <Block flex style={styles.profile}>
          <Block flex>
            <ImageBackground
              source={Images.BgHome}
              style={styles.profileContainer}
              imageStyle={styles.profileBackground}>
              <ScrollView
                showsVerticalScrollIndicator={false}
                style={{width, marginTop: '30%'}}>

                <Block row left>
                  <Image source={Images.Logo} style={styles.logo} />
                </Block>

                <Block flex row style={styles.profileCard}>
                  <Block flex>
                    <Image source={Images.ProfileNuril} style={styles.avatar} />
                  </Block>

                  <Block flex left style={styles.nameInfo}>
                      <Text
                        bold
                        size={28}
                        color="#32325D"
                        style={{
                          fontFamily: 'OpenSans-Bold',
                          fontWeight: 'bold',
                        }}>
                        MOHAMMAD NURIL UMAM
                      </Text>
                      <Text size={16} color="#32325D" style={{marginTop: 2}}>
                        .NET Programmer, IT
                      </Text>
                    </Block>
                </Block>

                <Block middle style={{marginTop: 30, marginBottom: 16}}>
                  <Block style={styles.divider} />
                </Block>

                <Block
                  row
                  style={{
                    paddingVertical: 14,
                    alignItems: 'baseline',
                  }}></Block>
              </ScrollView>
            </ImageBackground>
          </Block>
        </Block> */}

        <Footer>
          <FooterTab
            style={{
              backgroundColor: '#ffffff',
              borderTopWidth: 1,
              borderTopColor: '#eee',
            }}>
            <Button active vertical style={{backgroundColor: '#fff'}}>
              <Icon active name="apps" style={{color: '#434e52'}} />
              <Text style={{color: '#434e52'}}>Home</Text>
            </Button>
            <Button badge vertical>
              <Badge>
                <Text>2</Text>
              </Badge>
              <Icon name="document" style={{color: '#5b8c85'}} />
              <Text style={{color: '#5b8c85'}}>Activity</Text>
            </Button>
            <Button badge vertical>
              <Badge>
                <Text>5</Text>
              </Badge>
              <Icon name="mail" style={{color: '#5b8c85'}} />
              <Text style={{color: '#5b8c85'}}>Info</Text>
            </Button>
            <Button vertical>
              <Icon name="person" style={{color: '#5b8c85'}} />
              <Text style={{color: '#5b8c85'}}>Personal</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
