import {Dimensions, Platform} from 'react-native';
import {theme} from 'galio-framework';
import {HeaderHeight} from '../../constants/utils';
import {Right} from 'native-base';

const {width, height} = Dimensions.get('screen');
const thumbMeasure = (width - 48 - 32) / 3;

export default {
  
  headerBox: {
    backgroundColor: '#38251D',
    width: width,
    height: height/3,
  },

  headerBackground1: {
    width: width,
    height: height / 3,
  },

  headerBackground1Container: {
    width: width,
    height: height,
    padding: 0,
    zIndex: 1,
    opacity: .9,
  },

  headerBackground2Container: {
    width: width/1.7,
    height: 100,
    resizeMode: 'stretch',
    position: 'relative',
    opacity: .8,
  },

  profileName: {
    fontFamily: 'BentonSans-Bold',
    fontSize: 15
    // fontWeight: 'bold',
  },

  profileTitle: {
    marginTop: 3,
    fontFamily: 'OpenSans-Regular',
    fontSize: 12
    // fontWeight: 'bold',
  },

  profileBio: {
    fontFamily: 'OpenSans-Bold',
    fontSize: 13,
    marginTop: 5
  },

  profileBioVal: {
    fontFamily: 'OpenSans-Regular',
    fontSize: 12,
    marginTop: 5    
  },

  titleMenu : {
    fontFamily: 'BentonSans-Bold',
    fontSize: 12,
    marginTop: 5,
    color: '#6B5639',
    opacity: .5
  },

  

  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1,
  },
  profileContainer: {
    width: width,
    height: height,
    padding: 0,
    zIndex: 1,
  },
  profileBackground: {
    width: width,
    height: height / 2.7,
  },
  profileCard: {
    position: "relative",
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 10,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderRadius: 8,
    backgroundColor: '#ffffff',
    shadowColor: 'black',
    shadowOffset: {width: 5, height: 5},
    shadowRadius: 8,
    shadowOpacity: 5,
    zIndex: 2,   
    elevation: 2,

  },
  info: {
    paddingHorizontal: 40,
  },
  avatarContainer: {
    position: 'relative',
    marginTop: -10,
  },
  avatar: {
    width: 64,
    height: 64,
    borderRadius: 5,
    borderWidth: 0,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderRadius: 8,
    backgroundColor: '#ffffff',
    shadowColor: 'black',
    shadowOffset: {width: 5, height: 5},
    shadowRadius: 8,
    shadowOpacity: 5,
  },
  nameInfo: {
    marginTop: 35,
    fontFamily: 'OpenSans-Regular',
  },
  divider: {
    width: '100%',
    borderWidth: 1,
    borderColor: '#E9ECEF',
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure,
  },
  logo: {
    marginLeft: -15,
    marginTop: 0,
    marginBottom: 10,
  },
};
